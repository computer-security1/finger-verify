from .conn import create_connection
from .queries import create_table

DB_FILE = "./dumps/local.db"


if __name__ == "__main__":
    conn = create_connection(DB_FILE)

    if conn is None:
        print("Error! cannot create the database connection.")
    else:
        create_table(conn)
